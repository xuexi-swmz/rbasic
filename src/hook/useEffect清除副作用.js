import React, { useEffect, useState } from 'react'


function Test(){
  useEffect(()=>{
    let timer = setInterval(()=>{
      console.log('定时器');
    },1000)
    return ()=>{
      clearInterval(timer)
    }
  })
  return(
      <div>test</div>
    )
  }
export default function App() {
  const [flag,setFlag] = useState()
  return (
    <div>
      {flag?<Test/>:null}
      <button onClick={()=>setFlag(!flag)}>switch</button>
    </div>
  )
}
