import { useState } from 'react'

export default function useWindowScroll() {
  const [y,sety] = useState(0)
  window.addEventListener('scroll',()=>{
    const h = document.documentElement.scrollTop
    sety(h)
  })
  return [y]
}

/*  使用方法
import React from 'react'
import useWindowScroll from './hook/useWindowScroll'
export default function App() {
  const [y] = useWindowScroll()
  return (
    <div style={{height:'1000px'}}>
      {y}
    </div>
  )
}
*/