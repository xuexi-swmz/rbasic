import React, { useEffect, useState } from 'react'
import axios from 'axios'
  
export default function App() {
  useEffect(()=>{
    async function loadData(){
      // fetch
      fetch('http://geek.itheima.net/v1_0/channels').then(
        response=>response.json()
      ).then(data=>console.log(data))
      // axios
      const res = await axios.get('http://geek.itheima.net/v1_0/channels')
      console.log(res);
    }
    loadData()
  },[])
  return (
    <div>
    </div>
  )
}
