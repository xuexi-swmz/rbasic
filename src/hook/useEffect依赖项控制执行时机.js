import React, { useEffect } from 'react'
import { useState } from 'react'

/* 
  1. 不添加依赖项
    组件首次渲染执行一次，以及不管是哪个状态更改引起组件更新时都会重新执行
      1. 组件初始渲染
      2. 组件更新 （不管是哪个状态引起的更新）
  2. 添加空数组  组件只在首次渲染时执行一次
  3. 添加特定依赖项 副作用函数在首次渲染时执行，在依赖项发生变化时重新执行
*/

export default function App() {
  const [count,setCount] = useState(0)
  const [name,setName] = useState('1111')
  // useEffect 回调函数中用到的数据（比如，count）就是依赖数据，
  // 就应该出现在依赖项数组中，如果不添加依赖项就会有bug出现
  useEffect(()=>{
    document.title = count
  },[count])
  return (
    <div>
      {count}<button onClick={()=>setCount(count+1)}>+</button>
      <button onClick={()=>setName('cp')}>{name}</button>
    </div>
  )
}
