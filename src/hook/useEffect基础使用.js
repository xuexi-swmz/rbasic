import React, { useEffect } from 'react'
import { useState } from 'react'

export default function App() {
  const [count,setCount] = useState(0)
  useEffect(()=>{
    document.title = count
  })
  return (
    <div>
      {count}<button onClick={()=>setCount(count+1)}>+</button>
    </div>
  )
}
