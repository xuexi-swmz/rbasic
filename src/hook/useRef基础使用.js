import React, { useEffect, useRef, useState } from 'react'
  
class TestC extends React.Component{
  getName = ()=> {
    return 'getname'
  }
  state={
    name:'name'
  }
  
  render(){
    return(
      <div>类组件</div>
    )
  }
}


export default function App() {
  const testRef = useRef(null)
  const h1Ref = useRef(null)
  useEffect(()=>{
    console.log(testRef.current);
    console.log(h1Ref.current);
  },[])
  return (
    <div>
      <TestC ref={testRef} />
      <h1 ref={h1Ref}>h1</h1>
    </div>
  )
}
