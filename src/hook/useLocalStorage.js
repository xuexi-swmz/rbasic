import React, { useEffect, useState } from 'react'

export default function useLocalStorage(key,defaultValue) {
    const [message,setMessage] = useState(defaultValue)
    // 每次message变化，自动同步到本地localstorage
    useEffect(()=>{
        window.localStorage.setItem(key,message)
    },[message,key])
  return [message,setMessage]
}

// 使用方法
/* 
import React from 'react'
import useLocalStorage from './hook/useLocalStorage'
export default function App() {
  const [message,setMessage] = useLocalStorage('hookKey','ccc')
  setTimeout(()=>{
    setMessage('aaa')
  },3000)
  return (
    {message}
  )
}

*/