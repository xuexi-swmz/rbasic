import React, { useState } from 'react'

export default function App() {
  // function getCountValue(){
  //   for(let i =0;i<100;i++){
  //   }
  //   return '10';
  // }
  function Counter(props){
    const [count,setCount] = useState(()=>{
      return props.count
      // return getCountValue()
    })
    return(
      <button onClick={()=>setCount(count + 1)} >{count}</button>
    )
  }
  return (
    <div>
      <Counter count={10} />
    </div>
  )
}
