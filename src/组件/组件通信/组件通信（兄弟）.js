import React from "react";
// 兄弟通信  b->a
/* 
b传给app
app传给a
*/
function SonA(props){
  return(
    <div>
      this is SonA,{props.sendAMsg}
    </div>
  )
};

function SonB(props){
  const bMsg = 'b中的数据';
  return(
    <div>
      this is SonB
      <button onClick={()=>props.getBMsg(bMsg)}>发送数据到A</button>
    </div>
  )
}
class App extends React.Component {
  state={
    sendAMsg:'父传子初始信息'
  }
  getBMsg=(msg)=>{
    console.log(msg);
    this.setState({
      sendAMsg:msg,
    })
  }
  render() {
    return (
      <div>
        <SonA sendAMsg={this.state.sendAMsg} />
        <SonB getBMsg={this.getBMsg} />
      </div>
    );
  }
}

export default App;
