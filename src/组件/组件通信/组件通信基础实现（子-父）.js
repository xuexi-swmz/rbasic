import React from "react";
// 子传父：子组件调用父组件传来的函数，把想传递的数据当成函数的实参
/*
准备一个父函数传递给子组件
子组件调用父函数
子组件实参传给父函数  （子->父） 
 */
function Son(props) {
  const { getSonMsg } = props;
  function clickHandler(){
    const clickHandlerMsg = 'clickHandler函数传递的子组件'
    getSonMsg(clickHandlerMsg)
  }
  return (
    <div>
      this is Son
      {/* () => getSonMsg("子组件的msg") */}
      <button onClick={clickHandler}>click</button>
    </div>
  );
}

class App extends React.Component {
  state = {};
  getSonMsg = (sonMsg) => {
    console.log(sonMsg);
  };
  render() {
    return (
      <div>
        <Son getSonMsg={this.getSonMsg} />
      </div>
    );
  }
}

export default App;
