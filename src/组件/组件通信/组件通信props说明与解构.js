import React from "react";

function Son({message, list, userInfo, getMsg, child}) {
  // const {message, list, userInfo, getMsg, child} = props
  return (
    // props解构
    <div>
      函数子组件{message},
      {list.map((item) => (
        <p key={item}>{item}</p>
      ))}
      ,{userInfo.name}
      <button onClick={getMsg}>触发函数</button>
      {child}
    </div>

    // 太麻烦，通过解构props减少
    /* 
    <div>
      函数子组件{props.message},
      {props.list.map((item) => (
        <p key={item}>{item}</p>
      ))}
      ,{props.userInfo.name}
      <button onClick={props.getMsg}>触发函数</button>
      {props.child}
    </div> 
  */
  );
}
class App extends React.Component {
  state = {
    message: "this is msg",
    list: [1, 2, 3],
    userInfo: {
      name: "aa",
      age: 12,
    },
  };
  getMsg = () => {
    console.log("this is getMsg()");
  };
  render() {
    return (
      <div>
        <Son
          message={this.state.message}
          list={this.state.list}
          userInfo={this.state.userInfo}
          getMsg={this.getMsg}
          child={<span>this is span</span>}
        />
      </div>
    );
  }
}

export default App;
