import React from "react";
// 父传子
// 函数子组件
function SonF(props) {
  return <div>函数子组件,{props.message}</div>
}
// 类子组件
class SonC extends React.Component {
  render() {
    return <div>类子组件,{this.props.message}</div>;
  }
}
class App extends React.Component {
  state={
    message:'this is msg'
  }
  render() {
    return (
      <div>
        <SonF message={this.state.message} />
        <SonC message={this.state.message} />
      </div>
    );
  }
}

export default App;