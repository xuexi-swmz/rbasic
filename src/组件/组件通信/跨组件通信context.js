import React,{createContext} from "react";
// 跨组件通信
// app->A->C 可以直接App->c

const {Provider,Consumer} = createContext()
function ComA(){
  return(
    <div>
      this is ComA
      <ComC/>
    </div>
  )
}

function ComC(){
  return(
    <div>
      this is ComC
      <Consumer>
        {value=><span>{value}</span>}
      </Consumer>
    </div>
  )
}

class App extends React.Component {
  state={
    message:'this is msg'
  }
 
  render() {
    return (
      <Provider value={this.state.message}>

      <div>
        <ComA/>
      </div>
      </Provider>
    );
  }
}

export default App;
