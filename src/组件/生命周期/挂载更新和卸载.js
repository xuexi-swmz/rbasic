import React from "react";

class Test extends React.Component{
  timer=null
  componentDidMount(){
    this.timer=setInterval(()=>{
      console.log('定时器！启动！');
    },1000)
  }
  componentWillUnmount(){
    console.log('componentWillUnmount');
    clearInterval(this.timer)
  }
  render(){
    return <div>test</div>
  }
}

class App extends React.Component {
  constructor(){
    super()
    console.log('constructor');
  }
  state={
    count:0,
    flag:true
  }
  clickHandler=()=>{
    this.setState({
      count:this.state.count+1,
      flag:!this.state.flag
    })
  }
  componentDidUpdate(){
    console.log('componentDidUpdate');
  }
  componentDidMount(){
    console.log('componentDidMount');
  }
  // 不能在render里面调用setState，会引起函数一直更新
  render() {
    console.log('render');
    return(
      <div>
        {this.state.flag?<Test/>:null}
        <button onClick={this.clickHandler}>{this.state.count}</button>
        this is div
      </div>
    )
  }
}

export default App;
