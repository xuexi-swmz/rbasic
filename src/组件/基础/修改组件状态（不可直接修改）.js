// class fields

import React from "react";
class Counter extends React.Component{
  state={
    count:0,
    list:[1,2,3],
    person:{
      name:'zhangsan',
      age:20,
    },
  }
  changeState=()=>{
    this.setState({
      count:this.state.count+1,
      // list增加
      // list:[
      //   ...this.state.list,4,5
      // ],
      // 利用filter删除list的数据
      list:this.state.list.filter(item=>item!==2),
      person:{
        ...this.state.person,
        name:'lisi'
      },
    })
  }
  render(){
    return(
      <div>
        <ul>
          {this.state.list.map((item)=><li key={item}>{item}</li>)}
        </ul>
        <div>{this.state.person.name}</div>
        <button onClick={this.changeState}>{this.state.count}</button>
      </div>
    )
  }
}


function App() {
  return (
    <div className="App">
      <Counter/>
    </div>
  );
}

export default App;
