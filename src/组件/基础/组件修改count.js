// class fields

import React from "react";
class Counter extends React.Component{
  state={
    count:0,
  }
  changeCount=()=>{
    this.setState({
      count:this.state.count+1
    })
  }
  render(){
    return(
      <div>
        <button onClick={this.changeCount}>count+1{this.state.count}</button>
      </div>
    )
  }
}


function App() {
  return (
    <div className="App">
      <Counter/>
    </div>
  );
}

export default App;
