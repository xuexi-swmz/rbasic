// class fields

import React from "react";

class Counter extends React.Component {
  state = {
    msg: "this is msg",
  };
  inputChange = (e) => {
    console.log('input的值被修改了');
    this.setState({
      msg: e.target.value,
    });
  };
  render() {
    return (
      <input type="text" 
      value={this.state.msg} 
      onChange={this.inputChange} />
    );
  }
}

function App() {
  return (
    <div className="App">
      <Counter />
    </div>
  );
}

export default App;
