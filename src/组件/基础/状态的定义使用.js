import React from "react";

class TestComponent extends React.Component{
  // 1.定义组件状态
  state={
    name:'zhangsan'
  }
  // 回调函数更改数据 不可直接赋值
  changeName=()=>{
    this.setState({
      name:'lisi'
    })
  }
  render(){
    return(
      // 2.使用状态
      <div>
        当前name:{this.state.name}
        <button onClick={this.changeName} >修改name</button>

      </div>
    )
  }
}

function App() {
  return (
    <div className="App">
      <TestComponent/>
    </div>
  );
}

export default App;
