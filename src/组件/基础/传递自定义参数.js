

// 传递自定义参数
// 1.一个额外参数 {clickHandler} -> {()=>clickHandler('自定义参数')}
// 2.e和额外参数 {(e)=>clickHandler('自定义参数')}
function Hello(){
  const clickHandler = (e,msg) =>{
    e.preventDefault();
    console.log('事件触发',e,msg);
  }
  return(
    <a href="http://www.baidu.com/" onClick={(e)=>clickHandler(e,'msg')}>百度</a>
  )
}

function App() {
  return (
    <div className="App">
      <Hello />
    </div>
  );
}

export default App;
