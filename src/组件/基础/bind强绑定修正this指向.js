// 强绑定修正this指向

import React from "react";

class Test extends React.Component{
  // 强绑定this，将this指向修正到永远指向当前组件的实例对象
  constructor(){
    super()
    this.handler = this.handler.bind(this)
  }

  handler(){
    console.log(this);
    //this undefined  无法通过this.setState去改变数据
  }
  render(){
    return(
      <button onClick={this.handler}>click</button>
    )
  }
}

function App() {
  return (
    <div className="App">
      <Test/>
    </div>
  );
}

export default App;
