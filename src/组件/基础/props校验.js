import React from "react";
import PropType from "prop-types";
// props校验
/*
 npm i prop-types
  导入 prop-types
  函数组件使用默认值：1.使用defaultProps 2.函数参数默认值（推）
 */
function Test({list,pageSize=10}) {
  return (
    <div>
      {
      list.map((item) => (
        <p key={item}>{item}</p>
      ))
      }
      {pageSize}
    </div>
  );
}

// isRequired 必填
Test.prototype = {
  list: PropType.array,
};
// (1)
// Test.defaultProps={
//   pageSize:10
// }
class App extends React.Component {
  render() {
    return (
    <Test list={[1, 2, 3]} pageSize={20} />
  );
  }
}

export default App;
