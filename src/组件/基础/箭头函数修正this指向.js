// 箭头函数修正this指向

import React from "react";

class Test extends React.Component{

  handler(){
    console.log(this);
    //this undefined  无法通过this.setState去改变数据
  }
  render(){
    return(
      // 箭头函数使this沿用父函数render的this指向
      <button onClick={()=>this.handler()}>click</button>
    )
  }
}

function App() {
  return (
    <div className="App">
      <Test/>
    </div>
  );
}

export default App;
